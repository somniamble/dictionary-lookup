# README

This is a basic repack app (made through create-repack-app)

To start the back-end in development mode:
```
   (from top-level directory)
  $ rails s -p 3001
```

To start the front-end in development mode:
```
   (from client directory)
   $ yarn start
```

You'll need API secrets for Oxford Dictionary's API to run this locally.

Hosting through heroku coming soon!
