import React from 'react'
import {Header, Container} from 'semantic-ui-react'
import Entry from './Entry'
import {connect} from 'react-redux'

const EntryList = ({entries, word}) => (
  <Container>
    {word? <Header as="h2">Showing definitions for {word}</Header>: null}
    {entries.map(x => <Entry key={x["id"]} definition={x["definitions"][0]} {...x} />)}
  </Container>
)

const mapStoreToProps = (store) => ({
  entries: store.entries.entries,
  word: store.entries.word,
})

export default connect(mapStoreToProps)(EntryList)
