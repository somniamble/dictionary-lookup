import React from 'react'
import {Segment, Header} from 'semantic-ui-react'


const capitalize = (str) => str.charAt(0).toUpperCase() + str.substring(1)

const Entry = ({definition, examples=[], id}) => (
  <Segment secondary padded="very">
    <Header as="h3">{capitalize(definition)}</Header>
      {examples.map( (example, i) => <h4 key={id + i}><em>“{capitalize(example["text"])}”</em></h4> )}
  </Segment>
)

export default Entry
