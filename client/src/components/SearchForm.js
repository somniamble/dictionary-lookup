import React, {Component} from 'react'
import {Form} from 'semantic-ui-react'
import { getEntries } from '../store'
import { connect } from 'react-redux'

class SearchForm extends Component {

  state = {word: ""}

  initialize = () => this.setState({word: ""})

  handleChange = ({target: {value}}) => this.setState({word: value})

  handleSubmit = (e) => {
    e.preventDefault()
    let [{value}] = e.target
    let {dispatch} = this.props
    dispatch(getEntries(value))
  }

  render() {

    let { word } = this.state

    return (
      <Form style={{margin: "2em 0"}} onChange={this.handleChange} onSubmit={this.handleSubmit} size="large">
        <Form.Input placeholder="Enter search term" value={word} />
      </Form>
    )
    
  }
}

export default connect()(SearchForm)
