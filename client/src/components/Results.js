import React from 'react'
import {Loader} from 'semantic-ui-react'
import EntryList from './EntryList'
import {connect} from 'react-redux'

const Results = ({status, code}) => (
  status === "failure" ? (
    <h2 style={{color:"maroon"}}>{code === 404? "No Entries Found.": "Internal Server Error"}</h2>
  ) : status === "loading" ? (
    <Loader active indeterminate />
  ) : (
    <EntryList />
  )
)

const mapStoreToProps = (store) => ({
  status: store.status.status,
  code: store.status.code
})

export default connect(mapStoreToProps)(Results)
