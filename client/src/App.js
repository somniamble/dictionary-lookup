import React, { Component } from 'react';
import './App.css';
import {Header, Container, Divider} from 'semantic-ui-react'
import SearchForm from './components/SearchForm'
import Results from './components/Results'

class App extends Component {
  render() {
    return (
      <Container>
        <Header style={{margin: "3em 0 .125em 0"}}as="h1" size="huge">Dictionary Lookup</Header>
        <Divider />
        <SearchForm></SearchForm>
        <Results status="" />
      </Container>
    );
  }
}

export default App;
