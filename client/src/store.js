import { createStore, combineReducers, applyMiddleware, compose } from 'redux';
import thunk from 'redux-thunk';
import axios from 'axios';

// entries redux

const enhancers = compose(
  applyMiddleware(thunk),
  window.devToolsExtension ? window.devToolsExtension() : f => f
)

const ENTRIES = "ENTRIES"

const entries = (state = {}, action) => {
  switch (action.type) {
    case ENTRIES:
      return {word: action.word, entries: action.entries}
    default:
      return state;
  }
}

export const getEntries = (word) => (dispatch) => {
  dispatch({type: LOADING})
  axios.get(`/api/${word}`)
    .then( ({ data }) => {
      if (data.status !== undefined)
        dispatch({type: FAILURE, code: data.status})
      else {
        dispatch({type: ENTRIES, entries: data, word: word})
        dispatch({type: SUCCESS})
      } 
    })
    .catch( error => {
      console.log(error)
      dispatch({type: FAILURE, code: error.status})
    })
}

// status redux

const LOADING = "LOADING"
const FAILURE = "FAILURE"
const SUCCESS = "SUCCESS"

const status = (state = {}, action) => {
  switch (action.type) {
    case LOADING:
      return {status: "loading", code: 0}
    case FAILURE:
      return {status: "failure", code: action.code}
    case SUCCESS:
      return {status: "success", code: 200}
    default:
      return state;
  }
}

const rootReducer = combineReducers({
  entries,
  status,
})

const store = createStore(
  rootReducer,
  {entries: {entries: [], word: ''}, status: {status: "success", code: 200}},
  enhancers
)

if (module.hot){
  module.hot.accept('./', () => {
    const nextRootReducer = require('./store.js').default;
    store.replaceReducer(nextRootReducer);
  })
}

export default store;
