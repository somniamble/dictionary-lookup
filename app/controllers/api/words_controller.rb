class Api::WordsController < ApplicationController

  BASE_URL = 'https://od-api.oxforddictionaries.com/api/v1'
  LEMATRON_PREFIX =  '/inflections/en'

  def show
    word = params[:word]
    app_id = Rails.application.secrets.app_id
    api_key = Rails.application.secrets.api_key
    unless word.empty?
      url = "#{BASE_URL}/entries/en/#{word}"
      puts url
      response = HTTParty.get(url, {
        headers: {
          "Accept" => "application/json",
          "app_id" => app_id,
          "app_key" => api_key
        }
      })

      if response.code == 200
        
        # Get hash out of our response
        res = JSON.parse(response.to_json).with_indifferent_access
        # traverse hash to pull out a list of lexical entries for our word
        entries = res[:results][0][:lexicalEntries].map {
          |x| x[:entries].map { 
            |y| y[:senses] } }.flatten.select { |z| z.has_key?(:definitions) }

        render json: JSON.generate(entries)
      
      elsif response.code == 404
        render json: {errors: "No entries for #{word}", status: 404}
      else
        render json: {errors: "Internal Server Error", status: 500}
      end
    else
      render json: {errors: "Empty word", status: 404}
    end
  end
end
