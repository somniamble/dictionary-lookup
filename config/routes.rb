Rails.application.routes.draw do
  namespace :api do
    get ':word', action: :show,  controller: 'words'
  end

  #Do not place any routes below this one
  get '*other', to: 'static#index'
end
